export default class SWApiService {
  _apiBase = "https://swapi.dev/api";

  async getResource(url) {
    const res = await fetch(`${this._apiBase}${url}`);
    if (!res.ok) {
      throw new Error(`не смог получить ${url}  , вернулся ${res.status}`);
    }
    return await res.json();
  }

  _exstractId(item) {
    const idRegExp = /\/([0-9]*)\/$/;
    return item.url.match(idRegExp)[1];
  }

  //people
  async getAllPeople() {
    const result = await this.getResource(`/people/`);
    return result.results.map(this._tranformPeople);
  }

  async getPeople(id) {
    const people = await this.getResource(`/people/${id}`);
    return this._tranformPeople(people);
  }

  _tranformPeople=(people)=> {
    return {
      id: this._exstractId(people),
      name: people.name,
      gender: people.gender,
      birthYear: people.birth_year,
      eyeColor: people.eye_color,
      mass: people.mass,
      height: people.height,
      hairColor: people.hair_color,
    };
  }

  //planet
  async getAllPlanet() {
    let result = await this.getResource(`/planets/`);
    return result.results.map(this._transformPlanet);
  }

  async getPlanet(id) {
    const planet = await this.getResource(`/planets/${id}`);
    return this._transformPlanet(planet);
  }

  _transformPlanet(planet) {
    return {
      id: this._exstractId(planet),
      name: planet.name,
      population: planet.population,
      rotationPeriod: planet.rotation_period,
      diameter: planet.diameter,
    };
  }

  //starships
  async getAllStarships() {
    let result = await this.getResource(`/starships/`);
    return result.results;
  }

  getStarship(id) {
    return this.getResource(`/starships/${id}`);
  }
}
