import React from 'react'
import './ErrorBlock.css'

const ErrorBlock = () => {
  return (
    <div className="error-block">
      <h1>Возникли неполадки при загрузке.</h1> 
      <p>Мы уже реботаем над решением данной проблемы</p>
    </div>
  )
}

export default ErrorBlock
