import React, { Component } from "react";
import "./ItemList.css";
import SwapiService from "../../services/SwapiService";
import Loader from "../Loader/Loader";

export default class ItemList extends Component {
  state = {
    peopleList: null,
  };
  swApi = new SwapiService();

  componentDidMount() {
    this.swApi.getAllPeople().then((peopleList) => {
      this.setState({ peopleList });
    });
  }

  renderItems(arr) {
    return arr.map(({ id, name }) => {
      return (
        <li
          key={id}
          onClick={() => {
            this.props.onItemSelected(id);
          }}
        >
          {name}
        </li>
      );
    });
  }

  render() {
    const { peopleList } = this.state;

    if (!peopleList) {
      return <Loader />;
    }
    const items = this.renderItems(peopleList);

    return <ul className="item-list">{items}</ul>;
  }
}
