/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import './Header.css'

const Header = () => {
  return (
    <header className="header">
      <div className="header-title">SW site</div>
      <nav className="header-navigation">
        <a href="#">People</a>
        <a href="#">Planets</a>
        <a href="#">Starships</a>
      </nav>
    </header>
  );
};

export default Header;
