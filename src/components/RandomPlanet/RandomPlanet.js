import React, { Component } from "react";
import "./RandomPlanet.css";
import SwapiService from "../../services/SwapiService";
import Loader from "../Loader/Loader";
import ErrorBlock from "../ErrorBlock/ErrorBlock";

export default class RandomPlanet extends Component {
  state = {
    planet: {},
    loading: true,
    error: false,
  };
  swApi = new SwapiService();

  componentDidMount() {
    this.updatePlanet();
    this.interval = setInterval(this.updatePlanet,5000)
  }
  
  updatePlanet = () => {
    const id = Math.floor(Math.random() * 16) + 2;
    this.swApi.getPlanet(id).then(this.onPlanetLoaded).catch(this.onError);
  };
  onPlanetLoaded = (planet) => {
    this.setState({ planet, loading: false });
  };
  onError = (err) => {
    this.setState({ error: true, loading: false });
  };

  componentWillUnmount(){
    clearInterval(this.interval);
  }

  render() {
    const { planet, loading, error } = this.state;
    return (
      <div className="random-planet">
        {loading ? (
          <Loader />
        ) : error ? (
          <ErrorBlock />
        ) : (
          <RandomPlanetView planet={planet} />
        )}
      </div>
    );
  }
}

const RandomPlanetView = ({ planet }) => {
  const { id, name, population, rotationPeriod, diameter } = planet;
  return (
    <>
      <img
        className="random-planet-img"
        src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`}
        alt=""
      />
      <ul className="random-planet-info">
        <li>
          <h1>{name}</h1>
        </li>
        <li>
          <p>
            Population: <span>{population}</span>
          </p>
        </li>
        <li>
          <p>
            Rotation period: <span>{rotationPeriod}</span>
          </p>
        </li>
        <li>
          <p>
            Diameter: <span>{diameter}</span>
          </p>
        </li>
        <li></li>
      </ul>
    </>
  );
};
