import React, { Component } from "react";
import "./App.css";
import Header from "../Header/Header";
import RandomPlanet from "../RandomPlanet/RandomPlanet";
import ErrorBlock from "../ErrorBlock/ErrorBlock";
import PeoplePage from "../PeoplePage/PeoplePage";

export default class App extends Component {
  state = {
    hasError: false,
  };

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return <ErrorBlock />;
    }
    return (
      <div className="App">
        <div className="container">
          <Header />
          <RandomPlanet />
          <PeoplePage />
        </div>
      </div>
    );
  }
}
