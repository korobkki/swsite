import React, { Component } from "react";
import PeopleDetails from "../PeopleDetails/PeopleDetails.js";
import ItemList from "../ItemList/ItemList";
import ErrorBlock from "../ErrorBlock/ErrorBlock";
export default class PeoplePage extends Component {
  state = {
    selectedItem: 3,
    hasError: false,
  };

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  onItemSelected = (id) => {
    this.setState({
      selectedItem: id,
    });
  };
  render() {
    if (this.state.hasError) {
      return <ErrorBlock />;
    }
    return (
      <div className="main">
        <ItemList onItemSelected={this.onItemSelected} />
        <PeopleDetails itemId={this.state.selectedItem} />
      </div>
    );
  }
}
