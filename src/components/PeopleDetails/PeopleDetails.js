import React, { Component } from "react";
import "./PeopleDetails.css";
import SWApiService from "../../services/SwapiService";
import Loader from "../Loader/Loader";
import ErrorBtn from "../ErrorBtn/ErrorBtn";

export default class PeopleDetails extends Component {
  state = {
    item: null,
    loading: false,
  };

  componentDidMount() {
    this.updateItem();
  }

  swApi = new SWApiService();

  updateItem() {
    const { itemId } = this.props;
    if (!itemId) {
      return;
    }
    this.swApi.getPeople(itemId).then((item) => {
      this.setState({ item, loading: false });
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.itemId !== prevProps.itemId) {
      this.setState({ loading: true });
      this.updateItem();
    }
  }

  render() {
    if (!this.state.item) {
      return (
        <div className="people-details">
          <span className="people-details-alert">
            {" "}
            Выберете элемент из списка слева
          </span>
        </div>
      );
    }
    const {
      id,
      name,
      gender,
      birthYear,
      eyeColor,
      mass,
      height,
      hairColor,
    } = this.state.item;
    const loading = this.state.loading;

    const item = (
      <>
        <img
          className="people-details-img"
          src={`https://starwars-visualguide.com/assets/img/characters/${id}.jpg`}
          alt=""
        />
        <ul className="people-details-info">
          <li>
            <h2>{name}</h2>
          </li>
          <li>
            <p>gender : {gender}</p>
          </li>
          <li>
            <p>Birth year: {birthYear}</p>
          </li>
          <li>
            <p>Eye color : {eyeColor}</p>
          </li>
          <li>
            <p>Mass: {mass}</p>
          </li>
          <li>
            <p>Height : {height}</p>
          </li>
          <li>
            <p>Hair color : {hairColor}</p>
          </li>
        </ul>
        <ErrorBtn />
      </>
    );
    return <div className="people-details">{loading ? <Loader /> : item}</div>;
  }
}
