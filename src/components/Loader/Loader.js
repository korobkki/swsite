import React from "react";
import "./Loader.css";
const Loader = () => {
  return (
    <div className="loader">
      <div className="loadingio-spinner-double-ring-43o4epgknf">
        <div className="ldio-gnhbsoqmutp">
          <div></div>
          <div></div>
          <div>
            <div></div>
          </div>
          <div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Loader;
